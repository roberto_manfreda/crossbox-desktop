#include <iostream>
#include <thread>
#include "global/global.hpp"
#include "utils/cmd/cmd.hpp"
#include "utils/log/log.hpp"
#include "copycats/firefox/firefoxcopycat.hpp"

// follow
// the
// fl0w...

void init() {
    global::setMacOS();
    global::setLinux();
    global::setWindows();
    global::setShowLogOutput(true);
    global::setShowDebugOutput(false);
    global::setShowErrorOutput(false);

    utils::logger::log("Detecting current OS...\n");
    utils::cmd::setCurrentOS();
    utils::logger::log("Current OS is: " + global::CURRENT_OS + "\n");
}

int choice() {
    std::string menu;
    menu += "+ - - - - - - - - - - M E N U - - - - - - - - - - +\n";
    menu += "| 1 -> Save on simulated_usb_key                  |\n";
    menu += "| 2 -> Restore from a key                         |\n";
    menu += "|                                                 |\n";
    menu += "| 999 -> do some test                             |\n";
    menu += "|                                                 |\n";
    menu += "|                                                 |\n";
    menu += "| Any key -> Exit                                 |\n";
    menu += "+ - - - - - - - - - - - - - - - - - - - - - - - - +\n";

    utils::logger::log(menu);

    int choice{0};
    utils::logger::log("Do your choice: ");
    std::cin >> choice;

    return choice;
}

void menu(int choice) {
    switch (choice) {
        case 1: {
            copycat::firefox::savePreferences();
            break;
        }
        case 2: {
            // TODO
            break;
        }
        case 999: {
            // THIS IS A TEST CASE
            break;
        }
        default: {
            break;
        }
    }
}

int main(int argc, char const *argv[]) {
    utils::logger::debug("PROGRAM_STARTED\n");

    init();
    menu(choice());

    utils::logger::debug("PROGRAM_FINISHED\n");
    return 0;
}