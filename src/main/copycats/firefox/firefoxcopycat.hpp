#ifndef FIREFOXCOPYCAT_HPP
#define FIREFOXCOPYCAT_HPP

namespace copycat::firefox {
    /**
    * Detects if there are Firefox preferences on the current machine and saves them
    */
    void savePreferences();
}

#endif // FIREFOXCOPYCAT_HPP