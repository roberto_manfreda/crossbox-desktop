#ifndef CMDUTILS_HPP
#define CMDUTILS_HPP
#include <string>

namespace utils::cmd {
    /**
    * Retrieve the content of filename, logged by the exec() method
    *
    * @param the filename from where retrieve the output
    * @return the content of the file
    */
    std::string getCommandOutput(const std::string &fileName);

    /**
    * Exec a command using the system() method
    *
    * @param the command to execute
    * @return the output of the command
    */
    std::string exec(std::string command);

    /**
     * Helper method used by setCurrentOS()
     *
     * @param commandResult the output of executed command by setCurrentOS()
     * @param strToFind Platform specific str
     * @param OS name of the platform
     * @return OS
     */
    std::string filterOSOut(std::string &commandResult, const std::string &strToFind, const std::string &OS);

    /**
     * Autodetect the current OS and set the global value CURRENT_OS
     */
    void setCurrentOS();

    /**
    * Get the PID of a given process name
    *
    * @param processName the name of the process to get the PID
    * @return the process PID if exists, else return -1
    */
    int getProcessPID(const std::string &processName);

    /**
    * Kill a specific process (if exists), specyfing his name
    *
    * @param processName the process to kill
    *
    * @return 1 if the process exists and is killed with success or if the process doesn't exists
    *     else
    * @return 0 if the user won't kill the process
    */
    int killProcess(const std::string &processName);
}

#endif // CMDUTILS_HPP