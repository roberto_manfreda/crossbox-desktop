#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <string>
#include <cstring>
#include <stdlib.h>
#include "../../global/global.hpp"
#include "../log/log.hpp"
#include "../string/stringutils.hpp"
#include "cmd.hpp"

namespace utils::cmd {

    std::string getCommandOutput(const std::string &fileName) {
        std::string appender;

        try {
            std::string line;
            std::ifstream infile(fileName);
            while (std::getline(infile, line)) {
                appender += (line + "\n");
            }
        }
        catch (...) {
            // TODO ADD BETTER ERROR HANDLING
            std::cout << "error" << std::endl;
        }

        return appender;
    }

    std::string exec(std::string command) {
        std::string logFile{"execution_log.txt"};

        auto cleanCmd = "echo \"\" > " + logFile;
        system(cleanCmd.data());

        // pre-append "CMD /C " to exclude cygwin issues
        if (!global::CURRENT_OS.empty() && global::CURRENT_OS == global::WINDOWS) {
            command = "CMD /C " + command;
        }

        // redirect all output (2>&1 - stderr, stdout) on execution_log.txt to retrieve the content later
        command += " > " + logFile + " 2>&1";

        utils::logger::debug("Executing command: " + command + "\n");

        // exec the command
        system(command.data());

        // retrieving the content of execution_log.txt
        std::string output{getCommandOutput(logFile)};

        utils::logger::debug("Result:\n" + output + "\n");

        return output;
    }

    std::string filterOSOut(std::string &commandResult, const std::string &strToFind, const std::string &OS) {
        size_t found{commandResult.find(strToFind)};
        if (!commandResult.empty() && found != std::string::npos) {
            unsigned long pos{commandResult.find(':')};
            commandResult = commandResult.substr(pos + 1, strlen(commandResult.data()));
            utils::string::trim(commandResult);
            utils::logger::debug("FOUND OS is: " + commandResult + "\n");

            found = commandResult.find(OS);
            // Current OS is OS, returning it
            if (found != std::string::npos) {
                return OS;
            }
        }

        return "not_found";
    }

    void setCurrentOS() {
        // MacOS DETECTION
        global::setCurrentOS(global::MACOS);
        std::string command {"sw_vers"};
        auto commandResult {exec(command)};
        auto ret {filterOSOut(commandResult, "ProductName:", global::MACOS)};
        utils::string::trim(ret);
        if (ret == global::MACOS) {
            return;
        }

        // TODO LINUX DETECTION impl
        /*
        global::setCurrentOS(global::LINUX);
        command = "";
        commandResult = exec(command);
        ret = filterOSOut(commandResult, "???????????:", "Linux????????");
        utils::string::trim(ret);
        if (ret == "Linux????????") {
            return;
        }
         */

        // Windows detection
        global::setCurrentOS(global::WINDOWS);
        command = "CMD /C systeminfo | findstr /B /C:\"OS Name\"";
        commandResult = exec(command);
        ret = filterOSOut(commandResult, "OS Name:", global::WINDOWS);
        utils::string::trim(ret);
        if (ret == global::WINDOWS) {
            return;
        }

        if (ret == "not_found") {
            std::string error {"ERROR detecting current OS. Please report this error to the developer.\n"};
            utils::logger::log(error);
            exit(0);
        }
    }

    int getProcessPID(const std::string &processName) {
        int processPID{-1};
        std::string strProcessPID;
        std::string commandResult;

        // MacOS //TODO Test it because some calls was moved in the Common_ops block
        if (global::CURRENT_OS == global::MACOS) {
            std::string command {"ps -x | grep -i " + processName};
            commandResult = exec(command);

            // Filtering output: removing useless rows
            std::string filtered;
            while (commandResult.find('\n')) {
                unsigned long pos{commandResult.find('\n')};

                std::string sub{commandResult.substr(0, pos)};
                std::cout << "sub: " << sub << "\n";
                size_t found{sub.find("grep")};
                if (found == std::string::npos) {
                    filtered += sub;
                }

                commandResult = commandResult.substr(pos, strlen(commandResult.data()));
            }

            if (!filtered.empty()) {
                utils::logger::debug("Filtered command output is:\n" + filtered + "\n");
                commandResult = filtered;
            }
        }

        // Linux
        if (global::CURRENT_OS == global::LINUX) {
            /*Not Implemented*/
        }

        // Windows
        if (global::CURRENT_OS == global::WINDOWS) {
            auto command{"tasklist | findstr " + processName};
            commandResult = exec(command);

            size_t pos {commandResult.find('\n')};
            // Found process
            if (pos != std::string::npos) {
                commandResult = commandResult.substr(0, pos);
                // removing process name
                utils::string::trim(commandResult);
                pos = commandResult.find(' ');
                if (pos != std::string::npos) {
                    commandResult = commandResult.substr(pos, commandResult.size());
                }
            }
            utils::logger::debug(commandResult);
            /*Not Implemented*/
        }

        //COMMON_OPS _______________________
        // Exit, process is not in execution
        if (commandResult.empty()) return processPID;

        // Splitting the output to retrieve the string value of the PID
        size_t length{strlen(commandResult.data())};
        std::vector<std::string> words;
        std::string temp;
        for (int i = 0; i < length; i++) {
            if (commandResult[i] != ' ') {
                temp.push_back(commandResult[i]);
            } else {
                if (!temp.empty()) {
                    words.push_back(temp);
                    temp = "";
                }
            }
        }

        // The string PID value is the first element of the array
        if (!words.empty()) {
            try {
                processPID = std::stoi(words[0]);
            } catch (std::invalid_argument const &e) {
                utils::logger::error("Bad input: std::invalid_argument thrown.\n");
            } catch (std::out_of_range const &e) {
                utils::logger::error("Bad input: std::out_of_range thrown.\n");
            }
        }

        return processPID;
    }

    int killProcess(const std::string &processName) {
        int pid{getProcessPID(processName)};

        if (pid != -1) {
            std::string message{
                    "The process (" + processName + ") seems to exists. PID is: " + std::to_string(pid) + "\n"};
            utils::logger::debug(message);

            message = processName + " is executing. I need to close it in order to continue (i'm waiting here, save "
                                    "your data). Can i kill it?\ny/n (case-sensitive): ";
            utils::logger::log(message);

            char choice;
            std::cin >> choice;
            if (choice == 'n') {
                utils::logger::log("Storing " + processName + " preferences will be skipped.\n");
                // User won't kill the process so we need to skip saving the preferences! Returning 0.
                return 0;
            }
        }

        // The user agrees to kill the process, kill it (if exists) and return 1
        utils::logger::debug("Killing PID: " + std::to_string(pid) + "...\n");

        if (global::CURRENT_OS == global::MACOS) {
            std::string command{"kill " + std::to_string(pid)};
            exec(command);
        }
        if (global::CURRENT_OS == global::LINUX) {
            /*Not Implemented*/
        }
        if (global::CURRENT_OS == global::WINDOWS) {
            std::string command{"taskkill /F /PID " + std::to_string(pid)};
            exec(command);
        }

        utils::logger::debug("PID " + std::to_string(pid) + " killed.\n");
        return 1;
    }
}
