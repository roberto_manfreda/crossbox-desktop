#include <string>
#include "stringutils.hpp"

namespace utils::string {
    void trim(std::string &string)  {
        if (string.empty()) return;

        // Trim spaces from left side
        while (string.find(' ') == 0) {
            string.erase(0, 1);
        }

        // Trim spaces from right side
        size_t len = string.size();
        while (string.rfind(' ') == --len) {
            string.erase(len, len + 1);
        }
    }
}