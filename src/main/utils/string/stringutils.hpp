#ifndef STRINGUTILS_HPP
#define STRINGUTILS_HPP
#include <string>

namespace utils::string {

    /**
    * Trim initial ad final white-spaces of a string
    *
    * @param string
    */
    void trim(std::string &string);
}

#endif // STRINGUTILS_HPP

