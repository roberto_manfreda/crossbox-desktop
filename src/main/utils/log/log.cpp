#include <iostream>
#include "../../global/global.hpp"
#include "log.hpp"

namespace utils::logger {
    void log(std::string const &message) {
        if (global::SHOW_LOG_OUTPUT) std::cout << message;
    }

    void debug(std::string const &message) {
        if (global::SHOW_DBG_OUTPUT) std::cout << message;
    }

    void error(std::string const &message) {
        if (global::SHOW_ERR_OUTPUT) std::cout << message;
    }
}