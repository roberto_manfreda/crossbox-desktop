#ifndef LOG_HPP
#define LOG_HPP
#include <string>

namespace utils::logger {
    /**
     * Log user messages
     * @param message the message to log
     */
    void log(std::string const &message);

    /**
     * Log debug messages
     * @param message the message to log
     */
    void debug(std::string const &message);

    /**
     * Log error messages
     * @param message the message to log
     */
    void error(std::string const &message);
}

#endif // LOG_HPP
