#include <utility>
#include "global.hpp"

namespace global{
    void setMacOS() {
        MACOS = "Mac";
    }
    void setLinux() {
        LINUX = "Linux";
    }
    void setWindows() {
        WINDOWS = "Windows";
    }

    void setCurrentOS(const std::string &value) {
        CURRENT_OS = value;
    }

    void setShowLogOutput(const bool &value) {
        SHOW_LOG_OUTPUT = value;
    }
    void setShowDebugOutput(const bool &value) {
        SHOW_DBG_OUTPUT = value;
    }
    void setShowErrorOutput(const bool &value) {
        SHOW_ERR_OUTPUT = value;
    }
}