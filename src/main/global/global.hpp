#ifndef GLOBAL_HPP
#define GLOBAL_HPP
#include <string>

// TODO Are these global values handled in the correct way?!?
namespace global {
    // Global values
    inline std::string MACOS;
    inline std::string LINUX;
    inline std::string WINDOWS;

    inline std::string CURRENT_OS;

    // TODO set to false in production
    // 1.to hide infos - 2.to improve performance
    inline bool SHOW_LOG_OUTPUT;    // User messages
    inline bool SHOW_DBG_OUTPUT;    // Debug messages
    inline bool SHOW_ERR_OUTPUT;    // Error messages
    //_____

    // Setters
    void setMacOS();
    void setLinux();
    void setWindows();
    void setCurrentOS(const std::string &value);

    void setShowLogOutput(const bool &value);
    void setShowDebugOutput(const bool &value);
    void setShowErrorOutput(const bool &value);
    //_____
}

#endif // GLOBAL_HPP